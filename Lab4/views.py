from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import Message_Form
from .models import Entry

def home(request):
	return render(request, "Home.html", {})

def aboutme(request):
	return render(request, "AboutMe.html", {})

def contact(request):
	return render(request, "Registration.html", {})

def objform(request):
    response = {}
    sched = Entry.objects.all()
    response = {
        "schedules" : sched
    }
    return render(request, "Schedule.html", response) #render ke page schedule

def addform(request):
    form = Message_Form(request.POST or None)
    response = {}
    if(request.method == "POST"):
        if (form.is_valid()):
            activity = request.POST.get('activity')
            category = request.POST.get('category')
            date = request.POST.get('date')
            time = request.POST.get('time')
            place = request.POST.get('place')
            print(activity, category, date, time, place)

        
            entry = Entry(activity=activity, category=category,
                          date=date, time=time, place=place)
            entry.save()

            return redirect("/schedules") 
        else:
            return render(request, "Form.html", response)

    else:
        response['form'] = form
        return render(request, 'Form.html', response)

def deletesched(request):
    Entry.objects.all().delete()
    response = {}
    return render(request,'Schedule.html',response)