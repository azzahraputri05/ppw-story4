#FILE INI BIKIN SENDIRI:)

from django.conf.urls import url
from django.urls import path
from . import views
from django.conf.urls.static import static

app_name = 'Lab4'

urlpatterns = [
	path('',views.home,name='home'),
	path('about',views.aboutme,name='aboutme'),
	path('forms',views.addform,name="forms"),
	path('schedules',views.objform,name='schedules'),
	path('delete',views.deletesched,name='delete')
] 