from django import forms
from .models import Entry

class Message_Form(forms.Form):
	date = forms.DateField(label='Date', required=True, widget=forms.DateInput(attrs={'type' : 'date'}))
	time = forms.TimeField(label='Time', required=True, widget=forms.TimeInput(attrs={'type' : 'time'}))
	place = forms.CharField(label='Place', required=True)
	activity = forms.CharField(label='Activity',required=True,max_length='25')
	category = forms.CharField(label='Category', required=True)